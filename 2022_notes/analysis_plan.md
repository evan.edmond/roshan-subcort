# Thalamic volume group level differences - rerun
- latest genetics results awaited

# Volume correlated with overall motor-network connectivity
- get volume from subcort_seg
- generate group level motor network mask - threshold IC map - what threshold?
- fslstats mean with mask generated above
- Plot scatterplot - separate scatterplots for each group (ALS, con, FDR+, FDR-)

# Thalamic timecourse correlated with motor-network timecourse
- single subject
    - select thalamus mask from subcort_seg
    - use fslmeants on filtered_func_data_clean to get mean timecourse using this data
    - get mean timecourse for motor network.
        - dual regression output
    - get correlation coefficient
- plot boxplot
- between group stats
