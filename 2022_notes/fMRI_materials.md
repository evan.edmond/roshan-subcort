Materials referenced from FMRIB graduate course
(https://open.win.ox.ac.uk/pages/fslcourse/website/online_materials.html)

# fMRI 1: Statistics & Task fMRI: Basic GLM and Single-Subject Analysis 
- review 11 and 12 - basics of experiments and preprocessing, 13/14 optional

# fMRI 2: Statistics & Task fMRI: Inference and Group Analysis
- optional

# Resting State (ICA): Resting-State fMRI: ICA and Dual Regression
- all sections
