#!/bin/bash

subj=$1
wdir=$2

cd $wdir

#fsl anat step
fsl_anat -i ${subj}_ses-01_T1w.nii

cd ${subj}_ses-01_T1w.anat

#Calculate subcortical segmentation volumes
#open -a "*T1w.anat*" -type f
fslmaths T1_subcort_seg.nii.gz -thr 10 -uthr 10 -bin Lthalamus_only
fslmaths T1_subcort_seg.nii.gz -thr 49 -uthr 49 -bin Rthalamus_only
fslmaths T1_subcort_seg.nii.gz -thr 11 -uthr 11 -bin Lcaudate_only
fslmaths T1_subcort_seg.nii.gz -thr 50 -uthr 50 -bin Rcaudate_only
fslmaths T1_subcort_seg.nii.gz -thr 12 -uthr 12 -bin Lputamen_only
fslmaths T1_subcort_seg.nii.gz -thr 51 -uthr 51 -bin Rputamen_only

vol_Lthal=$(fslstats Lthalamus_only.nii.gz -V| awk '{print$1;}')
vol_Rthal=$(fslstats Rthalamus_only.nii.gz -V| awk '{print$1;}')
vol_Lcaudate=$(fslstats Lcaudate_only.nii.gz -V| awk '{print$1;}')
vol_Rcaudate=$(fslstats Rcaudate_only.nii.gz -V| awk '{print$1;}')
vol_Lputamen=$(fslstats Lputamen_only.nii.gz -V| awk '{print$1;}')
vol_Rputamen=$(fslstats Rputamen_only.nii.gz -V| awk '{print$1;}')

echo "$subj, Left thalamus, $vol_Lthal" >> output.csv
echo "$subj, Right thalamus, $vol_Rthal" >> output.csv
echo "$subj, Left caudate, $vol_Lcaudate" >> output.csv
echo "$subj, Right caudate, $vol_Rcaudate" >> output.csv
echo "$subj, Left putamen, $vol_Lputamen" >> output.csv
echo "$subj, Right putamen, $vol_Rputamen" >> output.csv
