#!/usr/bin/env python3

import os
import subprocess
from pathlib import Path

single_subj_script = Path("/home/evan/Documents/Research/scripts/2020_roshan/single_subject.sh")
wdir = Path("/home/evan/working/2020_02_17_subcortical_seg")

for t1 in wdir.rglob("*T1w.nii"):
    print(f"run single subject analysis on {t1.name}")
    os.chdir(t1.parent)
    subprocess.run(f"bash {single_subj_script} {t1.parts[-3]} {t1.parent}", shell=True)
    #print(f"bash {single_subj_script} {t1.parts[-3]} {t1.parent}")
